//
//  ContentView.swift
//  Charades WatchKit Extension
//
//  Created by Денис Матвеев on 18/01/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var topics: [Topic] // Topics data
    @EnvironmentObject var gameData: GameData // Game engine instance
    
    var body: some View {
        // Topic carousel view
        List {
            ForEach(topics) { topic in
                NavigationLink(destination:
                GameView(topic: topic).environmentObject(GameData(words: topic.wordList))) {
                    TopicCell(topic: topic)
                }
                .listRowBackground(RoundedRectangle(cornerRadius: 10, style: .continuous).fill( topic.getColor())) // Workaround to style individual list items (it's tricky as hell)
            }
            
            Text("Coming soon…")
                .multilineTextAlignment(.center)
                .foregroundColor(Color.secondary)
            
        }
        .listStyle(CarouselListStyle())
        .navigationBarTitle(Text("Select topic"))
    }
    
    struct TopicCell: View {
        // List item view
        var topic: Topic
        
        var body: some View {
            HStack {
                Image(uiImage: UIImage(named: topic.imagePath)!).resizable()
                    .frame(width: 24.0, height: 24.0)
                    .padding(.horizontal)
                    .opacity(0.7)
                Text(topic.name)
                    .font(.subheadline)
                    .fontWeight(.medium)
                    .frame(height: 50)
                    .multilineTextAlignment(.leading)
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(topics: topicData)
    }
}
