//
//  GameView.swift
//  Charades WatchKit Extension
//
//  Created by Денис Матвеев on 19/01/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct GameView: View {
    var topic: Topic
    @State private var isDragging = false
    @State private var dragOffset = CGFloat.zero
    @EnvironmentObject var gameData: GameData
    
    var body: some View {
        ZStack {
            if gameData.isWordVisible {
                // Demonstrating the word for a while at the start
                WordView(word: gameData.currentWord(), dragOffset: $dragOffset)
            }
            
            if !(gameData.isWordVisible || gameData.roundCurrentTime < 0) {
                // Showing the main game timer after the word was demonstrated
                TimerControlsView()
                .offset(x: isDragging ? dragOffset : 0, y: 0)
            }
            
            if gameData.roundCurrentTime < 0 {
                // Showing the word again in the end
                EndRoundView()
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .topLeading) // Adaptable frame that takes all available space
        .edgesIgnoringSafeArea(.bottom)
        .gesture(drag) // Connecting swipe gesture
        .onDisappear() {
            // Starting new game when exit to topics
            self.gameData.nextGame()
        }
        .navigationBarTitle(topic.name)
    }
    
    var drag: some Gesture {
        // Handling swipe gesture to switch words
        DragGesture()
            .onChanged { value in
                self.dragOffset = value.translation.width // Saving the length of the drag
                self.isDragging = true
        }
            .onEnded { value in
                self.isDragging = false
                if self.dragOffset < 0 {
                    // If it's a swipe to left
                    self.dragOffset = 120
                    withAnimation(.easeOut, {
                        self.dragOffset = CGFloat.zero
                        self.gameData.nextRound()
                    })
                    
                }
                self.dragOffset = CGFloat.zero
        }
    }
}


struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(topic: topicData[1])
            .environmentObject(GameData(words: topicData[1].wordList))
    }
}
