//
//  TimerControlsView.swift
//  Charades WatchKit Extension
//
//  Created by Денис Матвеев on 19/01/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct TimerControlsView: View {
    @EnvironmentObject var gameData: GameData
    
    private let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        ZStack {
            Text(String(gameData.roundCurrentTime))
                .font(Font.monospacedDigit(.title)())
                .multilineTextAlignment(.center)
                .animation(nil)
                .onReceive(timer) { input in
                    if !self.gameData.isRoundPaused {
                        if self.gameData.roundCurrentTime >= 0 {
                            self.gameData.roundCurrentTime -= 1
                            self.gameData.setProgress()
                        } else {
                            self.gameData.isRoundPaused = true
                        }
                    }
                }
            
            TimerAnimation()
        }
        .transition(AnyTransition.asymmetric(insertion: .opacity, removal: .move(edge: .leading)).animation(.easeOut(duration: 0.4))) // Animation on showing up
        .onAppear() {
            // Playing haptics
            WKInterfaceDevice().play(.start)
            do {
                sleep(UInt32(CGFloat(1)))
            }
        }
    }
}

struct TimerControlsView_Previews: PreviewProvider {
    static var previews: some View {
        TimerControlsView()
            .environmentObject(GameData(words: topicData[1].wordList))
    }
}
