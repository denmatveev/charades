//
//  TimerAnimation.swift
//  Charades WatchKit Extension
//
//  Created by Денис Матвеев on 19/01/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct TimerAnimation: View {
    @EnvironmentObject var gameData: GameData
    
    let gradientStart = Color(UIColor(named: "TimerGradient_1")!)
    let gradientEnd = Color(UIColor(named: "TimerGradient_2")!)
    
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .stroke(LinearGradient(
                        gradient: .init(colors: [gradientStart, gradientEnd]),
                    startPoint: .init(x: 1, y: 1),
                    endPoint: .init(x: -0.2, y: 1)), lineWidth: 15)
                    .rotationEffect(.degrees(-90))
                    .opacity(0.15)
                
                Circle()
                    .trim(from: 0, to: gameData.roundProgress) // This property is animating
                    .stroke(LinearGradient(
                            gradient: .init(colors: [gradientStart, gradientEnd]),
                            startPoint: .init(x: 1, y: 1),
                            endPoint: .init(x: -0.2, y: 1)),
                            style: StrokeStyle(lineWidth: 15.0, lineCap: .round, lineJoin: .round))
                    .rotationEffect(.degrees(-90))
            }
            .animation(.linear(duration: 0.1))
            .padding(.all)
            .frame(maxHeight: .infinity) // Stretching the ring to max size
        }

    }

}

struct TimerAnimation_Previews: PreviewProvider {
    static var previews: some View {
        TimerAnimation()
            .environmentObject(GameData(words: topicData[1].wordList))
    }
}
