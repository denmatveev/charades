//
//  EndRoundView.swift
//  Charades WatchKit Extension
//
//  Created by Денис Матвеев on 19/01/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct EndRoundView: View {
    @EnvironmentObject var gameData: GameData

    var body: some View {
        VStack(alignment: .leading) {
            Text("The phrase was")
                .font(.caption)
                .foregroundColor(Color.secondary)
                .multilineTextAlignment(.leading)
                .layoutPriority(1)
            
            Text(String(gameData.currentWord()))
                .font(.title)
                .multilineTextAlignment(.leading)
                .minimumScaleFactor(0.5) // Scale down twice if necessary
            
            Spacer()
            
            VStack(alignment: .center) {
                Button(action: {
                    self.gameData.nextRound()
                }) {
                    Image(systemName: "forward.fill")
                        .font(.headline)
                }
                .buttonStyle(PlainButtonStyle())
                .padding(20)
                .background(Color.white.opacity(0.2))
                .clipShape(Circle())
            }
            .layoutPriority(1)
            .frame(maxWidth: .infinity)
        }
        .transition(AnyTransition.opacity)
        .onAppear() {
            // Playing haptics
            WKInterfaceDevice().play(.failure)
            do {
                sleep(UInt32(CGFloat(1)))
            }
        }
    }
}

struct EndRoundView_Previews: PreviewProvider {
    static var previews: some View {
        EndRoundView()
            .environmentObject(GameData(words: topicData[1].wordList))
    }
}
