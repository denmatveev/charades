//
//  WordView.swift
//  Charades WatchKit Extension
//
//  Created by Денис Матвеев on 19/01/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI
import CoreMotion

let motionManager = CMMotionManager()

struct WordView: View {
    var word: String
    
    @EnvironmentObject var gameData: GameData
    @Binding var dragOffset: CGFloat // For animation
    
    private let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(word)
                .font(.title)
                .multilineTextAlignment(.leading)
                .minimumScaleFactor(0.5) // Scale down twice if necessary
                .offset(x: dragOffset, y: 0)
            
            Spacer()
            
            // Small timer view
            VStack(alignment: .center) {
                Text("\(gameData.wordShowTimer)")
                    .multilineTextAlignment(.center)
                    .frame(width: 24.0, height: 24.0)
                    .background(Color.white.opacity(0.2))
                    .clipShape(Circle())
                    .onReceive(timer) { _ in
                        if self.gameData.isWordVisible {
                            if self.gameData.wordShowTimer > 0 {
                                self.gameData.wordShowTimer -= 1
                            } else {
                                // Show ended
                                self.gameData.isRoundPaused = false
                                self.gameData.isWordVisible = false
                            }
                        }
                    }
                }
                .layoutPriority(1) // Now timer is always visible while text is being truncated
                .frame(maxWidth: .infinity) // This stretches the whole stack to the width of the screen, so the text gets maximum space
        }
        .transition(AnyTransition.asymmetric(insertion: .move(edge: .trailing), removal: .opacity).animation(.easeInOut(duration: 0.3)))
    }
}

struct WordView_Previews: PreviewProvider {
    @State static var dragOffset = CGFloat.zero
    
    static var previews: some View {
        WordView(word: topicData[1].wordList[1], dragOffset: $dragOffset)
            .environmentObject(GameData(words: topicData[1].wordList))
    }
}
