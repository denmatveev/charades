//
//  GameData.swift
//  Charades WatchKit Extension
//
//  Created by Денис Матвеев on 19/01/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

final class GameData: ObservableObject  {
    @Published var isWordVisible = true // Is the word for guessing appearing on the screen
    @Published var wordShowTimer = 5 // Word show timer
    @Published var roundStartTime = 30 // Starting round time in seconds
    @Published var roundCurrentTime: Int = 60 // Current round timer
    @Published var roundProgress: CGFloat = 1.0 // Progress for progress animation
    @Published var isRoundPaused = true // Is a current being paused
    private var roundWords = [String?]() // Words in play
    private var currentWordIndex = 0
    
    static let wordShowTime = 5 // For how long in seconds we show the word
    
    init(words: [String?]) {
        roundWords.append(contentsOf: words)
        nextGame()
    }
    
    func currentWord() -> String {
        if let word = roundWords[currentWordIndex] {
            return word
        } else {
            return "Error with current word"
        }
    }
    
    func setProgress() {
        roundProgress = CGFloat(roundCurrentTime) / CGFloat(roundStartTime)
    }
    
    func nextGame() {
        currentWordIndex = 0
        roundWords.shuffle()
        prepareNextRound()
    }
    
    func nextRound() {
        prepareNextRound()
        if currentWordIndex < roundWords.count - 1 {
            currentWordIndex += 1
        } else {
            currentWordIndex = 0
        }
    }
    
    private func prepareNextRound() {
        roundCurrentTime = roundStartTime
        wordShowTimer = GameData.wordShowTime
        roundProgress = 1.0
        isRoundPaused = true
        isWordVisible = true
    }
}
