//
//  Topic.swift
//  Charades WatchKit Extension
//
//  Created by Денис Матвеев on 19/01/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//
import SwiftUI

extension Int: Identifiable {
    public var id: Int {
        self
    }
}

extension Color {
    init(hex: String) {
        let scanner = Scanner(string: hex)
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)

        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff


        self.init(red: Double(r) / 0xff, green: Double(g) / 0xff, blue: Double(b) / 0xff)

    }
}

struct Topic: Identifiable {
    var id: String {
        name
    }
    var name: String
    var colorHex: String
    var imagePath: String
    var wordList: [String]
    
    func getColor() -> Color {
        return Color.init(hex: colorHex)
    }
}

var topicData = [Topic](
    [
    Topic(
        name: "Hey Simp!",
        colorHex: "5B00C6",
        imagePath: "topics.simp",
        wordList: ["Helicopter", "Stapler", "Coffee", "Leather", "Helmet", "Cruise Ship", "Wrench", "Cane sugar", "Bell pepper", "Dessert", "Sunflower", "Volcano", "Seesaw", "Dodgeball", "Cricket"]),
    Topic(
        name: "Nerd-Con",
        colorHex: "891298",
        imagePath: "topics.nerd",
        wordList: ["Hooker-Gandalf working the corner",
        "Harry Potter eats Dobby",
        "Stupid Sexy Flanders",
        "Batman hot live chat",
        "Goku does his taxes",
        "Transformers stuck in traffic",
        "Comunist Bulbasaur",
        "Flying Spaghetti Monster",
        "Godzilla at a yoga class",
        "Teenage mutant ninja nuns",
        "Daenerys’s dragon gets towed",
        "Neo dodging food offers at Christmas table",
        "Chewbacca complaining at post office",
        "Dr.Manhattan orders a Gin tonic"]),
    Topic(
        name: "Breaking News!",
        colorHex: "E43A48",
        imagePath: "topics.news",
        wordList: ["Kim Kardashian",
        "Cristiano Ronaldo",
        "Miley Cirus",
        "Ariana Grande",
        "Lindsay Lohan",
        "Angelina Jolie",
        "Justin Bieber",
        "Kanye West",
        "Marilyn Manson",
        "Donald Trump",
        "Kaitlyn Jenner"]),
    Topic(
    name: "Eureka!",
    colorHex: "CD3357",
    imagePath: "topics.eurika",
    wordList: ["Michelangelo",
        "Nuclear Fusion",
        "Charles Darwin",
        "Alan Turing",
        "Static Electricity",
        "Salvador Dalí",
        "Frank Kafka",
        "Plato",
        "Chemical Reaction",
        "Cell mitosis",
        "Heart Transplant",
        "Plate tectonics",
        "Silica",
        "Uranus",
        "Hiatus",
        "Senate",
        "Vibration",
        "Communicating vessels",
        "Galleon",
        "Metaphor"]),
    Topic(
    name: "So META!",
    colorHex: "8B0A57",
    imagePath: "topics.meta",
    wordList: ["Schroedinger’s cat’s cat",
        "Da Vinci’s code bugs",
        "Bach to the Future",
        "Smoked Hamlet",
        "Santa Claws",
        "Zebra crossing the zebra crossing",
        "Carbonated-watermelon",
        "The “bread-wiener”",
        "Jehovah’s Witness witnesses"])
    ]
)
