//
//  HostingController.swift
//  Charades WatchKit Extension
//
//  Created by Денис Матвеев on 18/01/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView(topics: topicData)
    }
}
